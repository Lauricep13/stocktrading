module.exports = {
  env: {
    node: true,
    commonjs: true,
    es2021: true,
    es6: true
  },
  extends: [
    'airbnb-base'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module'
  },
  plugins: ['jest'],
  rules: {
    'class-methods-use-this': 0,
    'comma-dangle': ['error', 'never'],
    'import/prefer-default-export': 'off',
    'arrow-parens': [2, 'as-needed', { requireForBlockBody: false }],
    'consistent-return': 'off',
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'no-underscore-dangle': 'off',
    'object-curly-newline': 'off',
    indent: ['error', 2],
    'no-use-before-define': ['error', { functions: true, classes: true, variables: false }],
    'max-len': ['error', 200],
    ignoreChainWithDepth: 0,
    'newline-per-chained-call': ['error', { ignoreChainWithDepth: 5 }],
    'no-console': 'error'
  }
};
