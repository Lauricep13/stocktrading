const dynamoose = require('dynamoose');

const dynamooseSchema = new dynamoose.Schema({
  name: { type: String, haskKey: true },
  low: { type: Number },
  high: { type: Number },
  close: { type: Number },
  change1W: { type: Number },
  change1M: { type: Number },
  Perf3M: { type: Number },
  Perf6M: { type: Number },
  PerfYTD: { type: Number },
  Volatility: { type: Number },
  capturedOn: { type: String }
});

module.exports = () => dynamoose.model(process.env.STOCKS_TABLE_NAME, dynamooseSchema, { create: false });
