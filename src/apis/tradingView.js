const axios = require('axios');

const urls = {
  local: 'https://scanner.tradingview.com/',
  dev: 'https://scanner.tradingview.com/',
  test: 'https://scanner.tradingview.com/',
  prod: 'https://scanner.tradingview.com/'
};

/**
 * Scans Stocks
 * @param {Object} body
 * @returns {Promise<Object>}
 */
exports.scan = async (body) => {
  const { data } = await axios.default.post(`${urls[process.env.STAGE]}america/scan`, body);
  return data;
};
