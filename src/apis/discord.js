const axios = require('axios');

const urls = {
  local: 'https://discord.com/api/v8/',
  dev: 'https://discord.com/api/v8/',
  test: 'https://discord.com/api/v8/',
  prod: 'https://discord.com/api/v8/'
};

/**
 * Gets any oversold stocks
 * @param {Object} arg
 * @param {Number} arg.rsiBelow
 * @param {Number} arg.stochBelow
 * @param {Number} arg.minClosePrice
 * @returns {Promise<import('../typeDefs/Stocks').Stock[]>}
 */
exports.sendMessageToGuild = async ({ content, guildId } = {}) => {
  const { data } = await axios.default.post(
    `${urls[process.env.STAGE]}channels/${guildId}/messages`,
    { content },
    {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bot OTE3OTYwMDEwMzY4MTY3OTY3.YbAS4Q.TFLXzb15WocYeiuBvtYpuSbKuu4'
      }
    }
  );

  return data;
};
