const axios = require('axios');
// eslint-disable-next-line import/no-extraneous-dependencies
const AWS = require('aws-sdk');
const Logger = require('js-logger');
const dynamoose = require('dynamoose');
const safeJsonStringify = require('safe-json-stringify');
const { SlashCreator } = require('slash-create');

exports.setupEnvironment = () => {
  setupLogging();
  configureAxios();
  configureAws();
};

exports.initializeSlashCreator = ({ applicationId, publicKey, token }) => {
  const creator = new SlashCreator({
    applicationID: applicationId,
    publicKey: publicKey,
    token: token,
    maxSignatureTimestamp: 30000,
    requestTimeout: 30000,
    latencyThreshold: 30000
  });

  creator.on('debug', Logger.debug);
  creator.on('warn', Logger.warn);
  creator.on('error', Logger.error);
  creator.on('rawREST', request => {
    Logger.debug('Request:', safeJsonStringify(request));
  });

  return creator;
};

const setupLogging = () => {
  Logger.setDefaults({ defaultLevel: Logger.DEBUG });
};

const configureAws = () => {
  AWS.config.logger = console;

  const dynamodb = new AWS.DynamoDB();
  dynamoose.aws.ddb.set(dynamodb);
};

const configureAxios = () => {
  axios.default.interceptors.request.use(request => {
    Logger.debug('Starting Request', safeJsonStringify(request, null, 2));
    return request;
  });

  axios.default.interceptors.response.use(response => {
    Logger.debug('Response', safeJsonStringify(response, null, 2));
    return response;
  });
};
