const Logger = require('js-logger');

module.exports = (err, req, res) => {
  if (err.response) {
    Logger.error('Error: ', JSON.stringify(err.response, null, 2));
  } else {
    Logger.error('Error: ', JSON.stringify(err, null, 2));
  }

  res.status(500).json(err);
};
