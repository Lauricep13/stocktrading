const path = require('path');
const { AWSLambdaServer } = require('slash-create');
const config = require('./config');

config
  .initializeSlashCreator({ applicationId: process.env.APPLICATION_ID, publicKey: process.env.PUBLIC_KEY, token: process.env.TOKEN })
  .withServer(new AWSLambdaServer(module.exports, 'handler'))
  .registerCommandsIn({ dirname: path.join(`${__dirname}/`, 'commands'), filter: /\w+(?<!Base)Command\.js/ });
