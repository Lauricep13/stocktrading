resource "aws_apigatewayv2_api" "this" {
  name          = "stocks-${var.environment}"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_route" "this" {
  api_id             = aws_apigatewayv2_api.this.id
  route_key          = "POST /discord/webhook"
  authorization_type = "NONE"
  target             = "integrations/${aws_apigatewayv2_integration.this.id}"
}

resource "aws_lambda_permission" "invoke_api" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.publish_slash_command_event.arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.this.execution_arn}/*/*/*/*"
  statement_id  = "AllowAPIGatewayV2"
}

resource "aws_apigatewayv2_integration" "this" {
  api_id           = aws_apigatewayv2_api.this.id
  integration_type = "AWS_PROXY"

  connection_type        = "INTERNET"
  integration_method     = "POST"
  integration_uri        = aws_lambda_function.publish_slash_command_event.invoke_arn
  timeout_milliseconds   = 30000
  payload_format_version = "2.0"
}

resource "aws_apigatewayv2_stage" "this" {
  api_id      = aws_apigatewayv2_api.this.id
  name        = "$default"
  auto_deploy = true
}
