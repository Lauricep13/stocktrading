const tradingView = require('./apis/tradingView');

/**
 * Gets any oversold stocks
 * @param {Object} arg
 * @param {Number} arg.rsiBelow
 * @param {Number} arg.stochBelow
 * @param {Number} arg.minClosePrice
 * @returns {Promise<import('./typeDefs/Stocks').Stock[]>}
 */
exports.getOversoldStocks = async ({ rsiBelow, stochBelow, minClosePrice } = {}) => {
  const body = {
    filter: [
      { left: 'close', operation: 'greater', right: minClosePrice },
      {
        left: 'Volatility.D',
        operation: 'nempty'
      },
      {
        left: 'type',
        operation: 'in_range',
        right: ['stock', 'fund']
      },
      {
        left: 'subtype',
        operation: 'in_range',
        right: ['common', 'foreign-issuer', 'etf', 'etf,odd', 'etf,otc', 'etf,cfd']
      },
      {
        left: 'exchange',
        operation: 'in_range',
        right: ['AMEX', 'NASDAQ', 'NYSE']
      },
      {
        left: 'RSI',
        operation: 'less',
        right: rsiBelow
      },
      {
        left: 'is_primary',
        operation: 'equal',
        right: true
      },
      {
        left: 'Stoch.K',
        operation: 'less',
        right: stochBelow
      }
    ],
    options: {
      active_symbols_only: true,
      lang: 'en'
    },
    markets: ['america'],
    symbols: {
      query: {
        types: []
      },
      tickers: []
    },
    columns: ['name', 'low', 'high', 'close', 'change|1W', 'change|1M', 'close', 'Perf.3M', 'Perf.6M', 'Perf.YTD', 'Volatility.D'],
    sort: {
      sortBy: 'Volatility.D',
      sortOrder: 'desc'
    },
    range: [0, 150]
  };

  const data = await tradingView.scan(body);
  return formatOutput(data.data, body.columns);
};

const mappings = {
  'change|1W': 'change1W',
  'change|1M': 'change1M',
  'Perf.3M': 'Perf3M',
  'Perf.6M': 'Perf6M',
  'Perf.YTD': 'PerfYTD',
  'Volatility.D': 'Volatility'
};

const formatOutput = (stocks, columns) => stocks.map(stock => {
  const obj = {};
  for (let i = 0; i < columns.length; i++) {
    const column = columns[i];
    // Set the mappings if they exist
    if (mappings[column]) {
      obj[mappings[column]] = stock.d[i];
    } else {
      obj[column] = stock.d[i];
    }
  }
  obj.createdOn = new Date().toLocaleString();
  
  Logger.debug('Stocks after reformatted', JSON.stringify(obj, null, 2));
  return obj;
});
