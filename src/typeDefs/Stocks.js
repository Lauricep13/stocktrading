/**
 * @typedef {{
 *  'name': String,
 *  'low': String,
 *  'high': String,
 *  'close': String,
 *  'change1W': String,
 *  'change1M': String,
 *  'Perf3M': String,
 *  'Perf6M': String,
 * 'PerfYTD': String,
 * 'Volatility': String,
 * 'capturedOn': Date
 * }} Stock
 */

module.exports = {};
