const BaseCommand = require('./BaseCommand');

module.exports = class FindOversoldStocksCommand extends BaseCommand {
  constructor(creator) {
    super(creator, {
      name: 'find-oversold-stocks',
      description: 'Finds oversold stocks'
    });

    // Not required initially, but required for reloading with a fresh file.
    this.filePath = __filename;
  }

  async safeRun(ctx) {
    await ctx.send("Processing your request");
  }
};
