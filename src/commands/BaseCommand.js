const { SlashCommand } = require('slash-create');
const Logger = require('js-logger');
const safeJsonStringify = require('safe-json-stringify');
const { setupEnvironment } = require('../config');
const AWS = require('aws-sdk');

module.exports = class BaseCommand extends SlashCommand {
  async run(ctx) {
    try {
      setupEnvironment();

      Logger.debug('Context: ', safeJsonStringify(ctx, null, 2));

      this.ctx = ctx;

      // Override the send method becuase it has to get a response within 3 seconds or the interaction will timeout.
      // as a work around we override the send method with the api call.
      ctx.send = this.send;

      return await this.safeRun(ctx);
    } catch (error) {
      Logger.error('Error:', safeJsonStringify(error, null, 2));
      return `An error occurred while running the command. \n Error Message: ${error.message}`;
    }
  }

  async send(content, options) {
    const eventBridge = new AWS.EventBridge({ endpoint: process.env.DEFAULT_ENDPOINT_URL });
    const body = {
      content,
      options
    };

    const params = {
      Entries: [
        {
          Detail: JSON.stringify(body, null, 2),
          DetailType: this.ctx.commandName,
          EventBusName: process.env.EVENT_BUS_NAME,
          Source: 'stocks'
        }
      ]
    };

    await eventBridge.putEvents(params).promise();
  }

  /**
   * Called when the command produces an error while running.
   * @param err Error that was thrown
   * @param ctx Command context the command is running from
   */
  onError(err, ctx) {
    Logger.error('Context Error: ', safeJsonStringify(ctx, null, 2));
    Logger.error('Error: ', safeJsonStringify(err, null, 2));

    if (!ctx.expired && !ctx.initiallyResponded) return ctx.send(`An error occurred while running the command. \n Error Message: ${err.message}`, { ephemeral: true });
  }

  async safeRun() {
    throw new Error(`${this.constructor.name} doesn't have a safeRun() method.`);
  }
};
