(async () => {
  const path = require('path');
  const config = require('../config');
  const Logger = require('js-logger');
  const safeJsonStringify = require('safe-json-stringify');
  const AWS = require('aws-sdk');

  config.setupEnvironment();

  Logger.debug('Argv: ', `${safeJsonStringify(process.argv, null, 2)}`);

  const secretsmanager = new AWS.SecretsManager({ region: process.argv[3] });
  const { SecretString } = await secretsmanager.getSecretValue({ SecretId: process.argv[2] }).promise();
  const creds = JSON.parse(SecretString);

  config
    .initializeSlashCreator({ applicationId: creds.applicationId, publicKey: creds.publicKey, token: creds.token })
    .registerCommandsIn({ dirname: path.join(`${__dirname}/../`, 'commands'), filter: /\w+(?<!Base)Command\.js/ })
    .syncCommands();
})();
