#!/usr/bin/env bash

if [ -z "$1" ]
then
   echo "Missing environment variable";
   exit 1
fi

cd terraform

rm providers_override.tf
rm -d -r .terraform

# Format terraform files
terraform fmt

# Initialize terraform
terraform init --backend-config=./stages/$1/init.tfvars

# Deploy terraform resources to localstack
terraform apply --auto-approve --var-file ./stages/$1/apply.tfvars 