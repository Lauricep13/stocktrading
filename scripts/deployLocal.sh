#!/usr/bin/env cmd
readonly TERRAFORM_BUCKET="terraform-state"

# Create certs for nginx
sh ../scripts/generateCerts.sh

# Make sure docker compose is up
docker-compose up -d

# Create terraform overrides files
find ../terraform/local/overrides/*.tf | while read -r file; do cp $file ../terraform/$(basename ${file%.*})_override.tf; done

export AWS_DYNAMODB_ENDPOINT=http://localhost:4566
export AWS_S3_ENDPOINT=http://localhost:4566
export AWS_STS_ENDPOINT=http://localhost:4566
export AWS_IAM_ENDPOINT=http://localhost:4566

# Wait for the remote state bucket to exist
echo "Waiting until S3 remote bucket is created..."

until aws --endpoint-url=http://localhost:4566 s3api head-bucket --bucket $TERRAFORM_BUCKET 2>/dev/null
do
  sleep 1
done

echo "Modifying /etc/hosts file to include DNS resolution for s3 buckets to point to localhost"
for bucket in $(aws --endpoint-url=http://localhost:4566 s3api list-buckets | jq -r .Buckets[].Name); do
  ../scripts/manageEtcHosts.sh add "$bucket.localhost"
done

# Initialize terraform
echo "Initializing terraform"
cd ../terraform
tflocal init --backend-config=./stages/local/init.tfvars

# Deploy terraform resources to localstack
echo "Deploying resources"
tflocal apply --auto-approve --var-file=./stages/local/apply.tfvars