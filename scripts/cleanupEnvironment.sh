#!/usr/bin/env bash

# Remove Buckets from /etc/hosts
for bucket in $(aws --endpoint-url=http://localhost:4566 s3api list-buckets | jq -r .Buckets[].Name); do
  ../scripts/manageEtcHosts.sh remove "$bucket.localhost"
done

docker-compose down

docker container prune -f
docker network prune -f

find ../terraform/*_override.tf | while read -r fileName; do rm -rf $fileName; done
rm -rf ../terraform/.terraform
rm -rf ../terraform/src.zip
rm -rf ../terraform/.terraform.lock.hcl