#!/usr/bin/env bash
printf "Configuring localstack components..."

# Creates the s3 bucket for terraform state locking
aws --endpoint-url http://localhost:4566 s3 mb s3://terraform-state  

# Creates the dynamodb table for terraform state locking
aws --endpoint-url http://localhost:4566 dynamodb create-table --table-name terraformlock \
    --attribute-definitions AttributeName=LockID,AttributeType=S \
    --key-schema AttributeName=LockID,KeyType=HASH \
    --billing-mode PAY_PER_REQUEST
