# Stand loal environment

## Requirements

# Software
AWS
LocalStack Pro (To run locally)
Terraform
Nodejs
Docker
Gitlab

# Envirnment Variables
export TF_VAR_discordToken=
export TF_VAR_discordPublicKey=
export TF_VAR_discordApplicationId=

export LOCALSTACK_API_KEY=

# Running Locally
docker-compse-up -d

sh ../scripts/deployLocal.sh

# Deploy To Dev Aws
sh ./scripts/deployRemote.sh dev

# Get Api Url
aws --endpoint-url http://localhost:4566 apigateway get-rest-apis --query "items[?name==\`discord-bot-api\`].id" --output text --region us-east-1