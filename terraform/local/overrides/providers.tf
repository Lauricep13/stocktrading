terraform {
  backend "s3" {
    bucket = "terraform-state"
    key    = "stock-trading"
    region = "us-east-1"
    endpoint = "http://localhost:4566"
    dynamodb_endpoint = "http://localhost:4566"
    dynamodb_table = "terraformlock"
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    force_path_style            = true
    encrypt            = true
  }
}

provider "aws" {
  region     = "us-east-1"
  access_key = "local"
  secret_key = "local"
}