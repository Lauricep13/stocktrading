data "archive_file" "src" {
  type        = "zip"
  source_dir  = "../src"
  output_path = "src.zip"
}

resource "aws_secretsmanager_secret" "this" {
  name = "/${var.environment}/discords"
}

resource "aws_secretsmanager_secret_version" "this" {
  secret_id = aws_secretsmanager_secret.this.id
  secret_string = jsonencode({
    applicationId = var.discordApplicationId
    publicKey     = var.discordPublicKey
    token         = var.discordToken
  })
}

resource "aws_lambda_function" "publish_slash_command_event" {
  function_name    = "publishSlashCommandEvent-${var.environment}"
  handler          = "index.handler"
  role             = aws_iam_role.publish_slash_command_event_role.arn
  runtime          = "nodejs14.x"
  timeout          = 30
  filename         = data.archive_file.src.output_path
  source_code_hash = data.archive_file.src.output_base64sha256
  memory_size      = 256

  environment {
    variables = {
      STAGE          = var.environment
      EVENT_BUS_NAME = aws_cloudwatch_event_bus.this.name
    }
  }
}

data "aws_iam_policy_document" "AWSLambdaTrustPolicy" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "publish_slash_command_event_role" {
  name               = "publish_slash_command_event_role-${var.environment}"
  assume_role_policy = data.aws_iam_policy_document.AWSLambdaTrustPolicy.json
}

resource "aws_iam_role_policy_attachment" "publish_slash_command_event_policy" {
  role       = aws_iam_role.publish_slash_command_event_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_cloudwatch_log_group" "publish_slash_command_event" {
  name              = "/aws/lambda/${aws_lambda_function.publish_slash_command_event.function_name}"
  retention_in_days = 14
}

resource "aws_iam_policy" "publish_slash_command_event_policy" {
  name        = "publish_slash_command_event-${var.environment}"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:${data.aws_caller_identity.current.account_id}:*:*",
      "Effect": "Allow"
    },
    {
      "Action": [
        "events:PutEvents"
      ],
      "Resource": "arn:aws:events:*:${data.aws_caller_identity.current.account_id}:event-bus/${aws_cloudwatch_event_bus.this.name}",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.publish_slash_command_event_role.name
  policy_arn = aws_iam_policy.publish_slash_command_event_policy.arn
}

resource "aws_lambda_function" "find_oversold_stocks" {
  function_name    = "findOversoldStocks-${var.environment}"
  handler          = "index.handler"
  role             = aws_iam_role.find_oversold_stocks_role.arn
  runtime          = "nodejs14.x"
  timeout          = 30
  filename         = data.archive_file.src.output_path
  source_code_hash = data.archive_file.src.output_base64sha256
  memory_size      = 256

  environment {
    variables = {
      STAGE          = var.environment
      APPLICATION_ID = var.discordApplicationId
      PUBLIC_KEY     = var.discordPublicKey
      TOKEN          = var.discordToken
    }
  }
}

data "aws_iam_policy_document" "aws_lambda_function" {
  statement {
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "find_oversold_stocks_role" {
  name               = "find_oversold_stocks_role-${var.environment}"
  assume_role_policy = data.aws_iam_policy_document.AWSLambdaTrustPolicy.json
}

resource "aws_iam_role_policy_attachment" "find_oversold_stocks_policy" {
  role       = aws_iam_role.find_oversold_stocks_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_cloudwatch_log_group" "find_oversold_stocks" {
  name              = "/aws/lambda/${aws_lambda_function.find_oversold_stocks.function_name}"
  retention_in_days = 14
}

resource "aws_iam_policy" "find_oversold_stocks_policy" {
  name        = "find_oversold_stocks-${var.environment}"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:${data.aws_caller_identity.current.account_id}:*:*",
      "Effect": "Allow"
    },
    {
      "Action": [
        "dynamodb:PutItem",
        "dynamodb:GetItem",
        "dynamodb:DescribeTable"
      ],
      "Resource": "arn:aws:logs:${data.aws_caller_identity.current.account_id}:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "find_oversold_stocks_logs" {
  role       = aws_iam_role.find_oversold_stocks_role.name
  policy_arn = aws_iam_policy.find_oversold_stocks_policy.arn
}
