variable "environment" {
  type = string
}

variable "aws-region" {
  type    = string
  default = "us-east-1"
}

variable "discordToken" {
  type = string
}

variable "discordPublicKey" {
  type = string
}

variable "discordApplicationId" {
  type = string
}