resource "aws_dynamodb_table" "stocks" {
  name         = "stocks-${var.environment}"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "name"

  attribute {
    name = "name"
    type = "S"
  }
}

data "aws_caller_identity" "current" {}

