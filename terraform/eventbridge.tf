resource "aws_cloudwatch_event_bus" "this" {
  name = "stock-events"
}

resource "aws_cloudwatch_event_target" "find-oversold-stocks" {
  target_id = "find-oversold-stocks"
  rule      = aws_cloudwatch_event_rule.find-oversold-stocks.name
  arn       = aws_lambda_function.find_oversold_stocks.arn
}

resource "aws_cloudwatch_event_rule" "find-oversold-stocks" {
  name        = "find-oversold-stocks"
  description = "Event rule to detect when a request for new over sold stocks is requested"

  event_pattern = <<PATTERN
{
  "source": [
    "stocks"
  ]
}
PATTERN
}

resource "aws_cloudwatch_event_target" "all-logs" {
  rule      = aws_cloudwatch_event_rule.find-oversold-stocks.name
  target_id = "all-logs"
  arn       = aws_cloudwatch_log_group.all-logs.arn
}

resource "aws_cloudwatch_log_group" "all-logs" {
  name = "events-stocks"
}